/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01field;

import java.util.ArrayList;
import java.util.Scanner;

class Person{
    String name;
    int age;
    void print(){
        System.out.printf("Name: %s , age: %d\n", name, age);
        if (age>18) {
            System.out.println("The person is adult!");
            
        }
    }
}

public class Day01Field {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner input = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);
        ArrayList<Person> x=new ArrayList<>();
        int i=0;
        while(true) {
            System.out.printf("Enter name of person:");
            x.add(new Person());
            String xName=input.nextLine();
            x.get(i).name=xName;
                        if (xName.isEmpty()) {
                break;
            }
            System.out.printf("Enter age of person:");
            x.get(i).age=input.nextInt();
            input.nextLine();

            i++;
        }

        System.out.println("");
        shortest(x);
//        for (Person x1 : x) {
//            x1.print();
//        }
    }
    static void shortest(ArrayList<Person> a){
        Person shortest = a.get(0);
        for (Person p:a) {
            if((shortest.name.length()>p.name.length())&&!p.name.isEmpty()){
                shortest=p;
            }
        }
        System.out.print("Oldest person is: ");
        shortest.print();
    }
    
}
