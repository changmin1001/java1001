/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03carencap;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

class Car{
    private String makeModel;
    private int yearOfProduction;
    private double engineSizeL;
    private int carId;

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        if(carId<1){
            throw new IllegalArgumentException("invalid number < 1\n");
        }
        this.carId = carId;
    }
    
    Car(String makeModel,int yearOfProduction,double engineSizeL){
        setModel(makeModel);
        setProduction(yearOfProduction);
        setEngineSizeL(engineSizeL);
    }
    void setModel(String makeModel){
        if(makeModel.length()<3&&makeModel.length()>0){
            throw new IllegalArgumentException("makeModel must be at least 3 characters long\n");
        }
        this.makeModel=makeModel;
    }
    void setProduction(int yearOfProduction){
        if(yearOfProduction<1900||yearOfProduction>2100){
            throw new IllegalArgumentException("yearOfProduction must be between 1900 and 2100\n");
        }
        this.yearOfProduction=yearOfProduction;
    }
    void setEngineSizeL(double engineSizeL){
        if(engineSizeL<0||engineSizeL>10.0){
            throw new IllegalArgumentException("engineSizeL must be between 0 and 10.0\n");
        }
        this.engineSizeL=engineSizeL;
    }
    String getModel(){
        return makeModel;
    }
    int getProduction(){
        return yearOfProduction;
    }
    double getEngineSizeL(){
        return engineSizeL;
    }
    void print(){
        System.out.printf("Car: %s made in %d,with the engine %.2f\n", makeModel,yearOfProduction,engineSizeL);
    }
}

/**
 *
 * @author 1996018
 */
public class Day03CarEncap {
    static Scanner input =  new Scanner(System.in);
    static void AddCar(ArrayList<Car> parking) { 
        while(true){
            try{
                System.out.print("Please input your model: ");
                String model=input.nextLine();
                if (model.equals("")) {
                    System.out.println("Entry ended.");
                    System.out.println("");
                    break;
                }
                System.out.print("Please input car's year: ");
                int year=input.nextInt();
                System.out.print("Please input car's engine: ");
                double engineL=input.nextDouble();
                input.nextLine();
                Car car1= new Car(model,year,engineL);
                parking.add(car1);
                System.out.println("Car added!\n");

            }catch(IllegalArgumentException ex){
                System.out.println("Error: " + ex.getMessage());
            } 
        } 
    }
    static void ListAllCars(ArrayList<Car> parking) {
         for (int i = 0; i < parking.size(); i++) {
             System.out.print("#"+(i+1)+"  ");
             parking.get(i).print();
        }
    }

    static void ModifyCar(ArrayList<Car> parking) {
      //  while(true){
            try{
                System.out.print("Please input the car ID: ");
                int result = indexError(input.nextInt()-1, parking.size());
                Car modifier= parking.get(result);
                input.nextLine();
                modifier.print();

                System.out.print("Please input your model: ");
                modifier.setModel(input.nextLine());

                System.out.print("Please input car's year: ");
                modifier.setProduction(input.nextInt());
                System.out.print("Please input car's engine: ");
                modifier.setEngineSizeL(input.nextDouble());
                input.nextLine();
                modifier.print();
                System.out.println("Car updated!");
                //break;
            }catch(ArithmeticException ex){
                System.out.println("Error: " + ex.getMessage());
            } 
        //}
    }
    public static int indexError(int number1, int number2) {
        if (number1 > number2){
        throw new ArithmeticException("Index problem");
        }
        return number1;
    }
    static void DeleteCar(ArrayList<Car> parking) { 
        
//        if((input.nextInt()-1)>parking.size()){
//            throw new IllegalArgumentException("Out of range\n");
//        }
       // while(true){
            try{
                System.out.print("Please input the car ID: ");
                int carId=input.nextInt();
                int result = indexError(carId, parking.size());
                Car deletedCar= parking.get(result-1);
                input.nextLine();
                deletedCar.print();
                parking.remove(deletedCar);
                ListAllCars(parking);
                System.out.println("Deleted No."+result+" element");
      //          break;
            }catch(ArithmeticException ex){
                System.out.println("Error: " + ex.getMessage());
            } 
       // }
    }
    static void ComputeAndDisplayStatistics(ArrayList<Car> parking) { 
        Car oldest = parking.get(0);
        Car largetEngine = parking.get(0);
        //double largetEngine=Integer.MIN_VALUE;
        int sumYears=0;
        DateFormat df = new SimpleDateFormat("yyyy");
        Date dateobj = new Date();
        int currentYear=Integer.parseInt(df.format(dateobj));
        for (int i = 0; i < parking.size(); i++) {
            parking.get(i).print();
            sumYears+=(currentYear-parking.get(i).getProduction());
            Car c = parking.get(i);
            if (parking.get(i).getProduction()<oldest.getProduction()) {
                oldest=c;
            }
            if (parking.get(i).getEngineSizeL()>largetEngine.getEngineSizeL()) {
                largetEngine=c;
            }
        }
        System.out.println("The oldest year is: ");
        oldest.print();
        System.out.println("The largest engine is ");
        largetEngine.print();
        System.out.printf("The average age is %.2f\n",(double)(sumYears/parking.size()));
    }
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<Car> parking = new ArrayList<>();
        
        while(true){
            System.out.print("1. Add a Car\n2. List all Cars (numbered)\n3. Modify a Car\n4. Delete a Car\n5. Compute and display statistics\n0. Exit\n");
            System.out.print("Please input your choice: ");
            int choice = input.nextInt();
            input.nextLine();
            if (choice==0) {
                break;
            }
            else{
                switch(choice){
                    case 1:AddCar(parking);break;
                    case 2:ListAllCars(parking);break;
                    case 3:ModifyCar(parking);break;
                    case 4:DeleteCar(parking);break;
                    case 5:ComputeAndDisplayStatistics(parking);break;
                }
            }
        }

        if (parking.size() == 0) {
            System.out.println("Skipping statistical analysis - nothing in parking");
            return;
        }
        System.out.println("");

//        System.out.println("Parking contains:");
//        //int oldest=Integer.MAX_VALUE;

    }
    
}
