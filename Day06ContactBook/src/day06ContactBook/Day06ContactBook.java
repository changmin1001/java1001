package day06ContactBook;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

class Contact {
	String name; // ^[\w ,.-]{1,50}$ 1-50 charcters, only letters, digits, space, comma, dot,
					// minus are permitted
	Date dateOfBirth; // year 1900-2100, use String if you can't figure out Date
	String postalCode; // ^[a-zA-Z][\d][a-zA-Z][ ]?[\d][a-zA-Z][\d]$ must look like a valid postal code
						// e.g. A1A 1A1
	String phoneNumber; // ^([+]\d{3} )?\d{3}[-]\d{3}[-]\d{4}$ must be in one of two formats:
						// 514-555-1234 or with a +number prefix, e.g. +123 234-555-1234 where number is
						// 1-3 digits long; you should be able to use a single regular expression for
						// both cases

	public String getName() {
		return name;
	}

	public void setName(String name) throws InvalidDataException {
		if (!name.matches("[\\w ,.-]{1,50}")) {
			throw new InvalidDataException(
					"contact name only letters, digits, space, comma, dot, minus are permitted ");
		}
		this.name = name;
	}

	public Date getDateOfBirth() {

		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) throws InvalidDataException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateOfBirth);
		if (calendar.get(Calendar.YEAR) > 2100 || calendar.get(Calendar.YEAR) < 1900) {
			throw new InvalidDataException("Birthday date can only between 1900 and 2100! ");
		}
		this.dateOfBirth = dateOfBirth;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) throws InvalidDataException {
		if (!postalCode.matches("[a-zA-Z][\\d][a-zA-Z][ ]?[\\d][a-zA-Z][\\d]")) {
			throw new InvalidDataException("Postal Code is not valid.----e.g. A1A 1A1");
		}
		this.postalCode = postalCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) throws InvalidDataException {
		if (!phoneNumber.matches("([+]\\d{1,3} )?\\d{3}[-]\\d{3}[-]\\d{4}")) {
			throw new InvalidDataException("Phone Number format error.");
		}
		this.phoneNumber = phoneNumber;
	}

	void print() {
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-mm-dd");
		System.out.printf("User Name: %s, Birthday Date: %s, Post code: %s, Phone number: %s\n", name, formatter2.format(dateOfBirth) ,postalCode,phoneNumber);
		//}
	}//
}

class InvalidDataException extends Exception {
	InvalidDataException(String msg) {
		super(msg);
	}
}

public class Day06ContactBook {
	static ArrayList<Contact> namesList = new ArrayList<Contact>();
	static Scanner input = new Scanner(System.in);

	static String validate(String vString) throws InvalidDataException {
		if (vString.isEmpty()) {
			throw new InvalidDataException("The line information can not be empty!");
		}
		return vString;
	}
	static void print() {
		
		for (Contact c: namesList) {
			c.print();
		}
	}
	static void oldest() {
		Calendar calendar = Calendar.getInstance();
		Contact oldlestG=namesList.get(0);
		calendar.setTime(oldlestG.getDateOfBirth());
		//int oldest=calendar.get(Calendar.YEAR);
		for (Contact c: namesList) {
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(c.getDateOfBirth());
			if (calendar.get(Calendar.YEAR)>calendar2.get(Calendar.YEAR)) {
				oldlestG=c;
			}
		}
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-mm-dd");
		System.out.printf("Oldest Contact Name is: %s, Birthday Date: %s, Post code: %s, Phone number: %s\n", oldlestG.getName(),formatter2.format(oldlestG.getDateOfBirth()),oldlestG.getPostalCode(),oldlestG.getPhoneNumber());;
	}
	static void readFiles() {
		try (Scanner fileInput = new Scanner(new File("addressbook.txt"))) {

			while (fileInput.hasNextLine()) {
				try {
					String eString = validate(fileInput.nextLine());
					String[] infoList = eString.split(";");
					Contact p = new Contact();
					SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-mm-dd");
					Date date1 = formatter1.parse(infoList[1]);
					p.setDateOfBirth(date1);
					p.setName(infoList[0]);
					p.setPostalCode(infoList[2]);
					p.setPhoneNumber(infoList[3]);
					namesList.add(p);
				} catch (InvalidDataException | NumberFormatException ex) {
					System.out.println("Data error: " + ex.getMessage());
				}
			}
		} catch (Exception ex) {
			System.out.println("Error reading file: " + ex.getMessage());
		}

	}

	public static void main(String[] args) {
		readFiles();
		print();
		oldest();
	}

}
