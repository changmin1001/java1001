package day06TodoMenuPersistent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

class Todo {
	private	String task; // 1-50 characters composed of uppercase, lowercase letters, digits, spaces, minus, underscore, dot, comma, question mark, dollar sign, percentage, ampersand, any of: +-*/= 
	private	Date dueDate; // year range 1900-2100 inclusive - research Date and SimpleDateFormat
	private	boolean isDone;
	public String getTask() {
		return task;
	}
	public void setTask(String task)  throws InvalidDataException {
		if (!task.matches("[\\w ,.-]{1,50}")) {
			throw new InvalidDataException(
					"contact name only letters, digits, space, comma, dot, minus are permitted ");
		}
		this.task = task;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) throws InvalidDataException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dueDate);
		if (calendar.get(Calendar.YEAR) > 2100 || calendar.get(Calendar.YEAR) < 1900) {
			throw new InvalidDataException("Birthday date can only between 1900 and 2100! ");
		}
		this.dueDate = dueDate;
	}
	public boolean isDone() {
		return isDone;
	}
	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

}


class InvalidDataException extends Exception {
	InvalidDataException(String msg) { super(msg); }
}

public class Day06TodoMenuPersistent {
	static Scanner input= new Scanner(System.in);
	static ArrayList<Todo> namesList = new ArrayList<Todo>();
	static void scanInput() throws ParseException {
		while (true) {
			System.out.println("1. Add todo\n2. List all todos (numbered)\n3. Modify todo (by number)\n4. Delete todo (by number)\n5. Display pending todos (prefix overdue with \"OVERDUE! \" string)\n0. Exit (and save all data to file)");
			int choice= input.nextInt();
			switch (choice) {
			case 0: System.out.println("You are log out!");return;
			case 1: addTodo(); break;
			case 2: listAll(); break;
			case 3: modifyToDo();break;
			case 4: deleteTODO();break;
			case 5: displayPending();break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + choice);
			}

		}
		
	}
	 static void displayPending() {

	}
	 static void deleteTODO() {
		 readFile();
		 System.out.println("Which one you want to delete: ");
		 int index= input.nextInt();
		 namesList.remove(index-1);
		 saveData();
	}
	 static void modifyToDo() throws ParseException {

		 readFile();
		 System.out.println("Which one you want to modified: ");
		 int indexM= input.nextInt();
		 try {
			 System.out.println("Please modifiy your task:");
			 namesList.get(indexM-1).setTask(input.nextLine());
			 System.out.println("Please modifiy your Due date:");
			 Date dueDate=new SimpleDateFormat("yyyy-mm-dd").parse(input.nextLine());
			 namesList.get(indexM-1).setDueDate(dueDate);
			 System.out.println("Please modifiy due:");
			 namesList.get(indexM-1).setDone(Boolean.parseBoolean(input.nextLine()));;
			 }
		 catch (InvalidDataException e) {
				System.out.println("Data error: " + e.getMessage());
		 }
		 saveData();
	}
	 private static void readFile() {
		// TODO Auto-generated method stub
		 namesList.clear();
		 //ArrayList<String> lines=new ArrayList<String>();
		 try (Scanner fileInput = new Scanner(new File("todos.txt"))) {
			 while (fileInput.hasNextLine()) {
			//	 lines.add(fileInput.nextLine());
				 String[] infoList = fileInput.nextLine().split(";");
					Date dueDate=new SimpleDateFormat("yyyy-mm-dd").parse(infoList[1]);
					Todo t=new Todo();
					try {
						t.setTask(infoList[0]);
						t.setDueDate(dueDate);
						t.setDone(Boolean.parseBoolean(infoList[2]));
						namesList.add(t);
					} catch (InvalidDataException e) {
						System.out.println("Data error: " + e.getMessage());
					}

			 }
			
		 }
		 catch (Exception ex) {
			System.out.println("Error reading file: " + ex.getMessage());
		}
	}
	static void listAll() {
		 try (Scanner fileInput = new Scanner(new File("todos.txt"))) {
			 while (fileInput.hasNextLine()) {
					String[] infoList = fileInput.nextLine().split(";");
					System.out.printf("Product: %s, Due Date: %s , Due: %s\n",infoList[0],infoList[1],infoList[2]);
			 }
		 }
		 catch (Exception ex) {
			System.out.println("Error reading file: " + ex.getMessage());
		}
		 
	}
	static void loadData() { 
		 try (PrintWriter fileOutput = new PrintWriter(new FileOutputStream(new File("todos.txt"), true))) {
			for (Todo t:namesList) {
				fileOutput.println(t.getTask()+";"+t.getDueDate()+";"+t.isDone());
			}
		} catch (Exception ex) {
			System.out.println("Error reading file: " + ex.getMessage());
		}

	}
	static void saveData() {
		 try (PrintWriter fileOutput = new PrintWriter(new FileOutputStream(new File("todos.txt")))) {
			for (Todo t:namesList) {
				fileOutput.println();
				fileOutput.println(t.getTask()+";"+t.getDueDate()+";"+t.isDone());
			}
		} catch (Exception ex) {
			System.out.println("Error reading file: " + ex.getMessage());
		}

	}
	static void addTodo()  throws ParseException {
		input.nextLine();
		System.out.print("Please input your menu item: \n");
		String item=input.nextLine();
		System.out.print("Please input item due date: \n");
		String due=input.nextLine();
		System.out.print("Please input item due date: \n");
		boolean isDone=input.nextBoolean();
		Date dueDate=new SimpleDateFormat("yyyy-mm-dd").parse(due);
		Todo t=new Todo();
		input.nextLine();
		try {
			t.setTask(item);
			t.setDueDate(dueDate);
			t.setDone(isDone);
			namesList.add(t);
		} catch (InvalidDataException e) {
			System.out.println("Data error: " + e.getMessage());
		}
		loadData();
	}
	public static void main(String[] args) throws ParseException {
		scanInput();
	}

}
