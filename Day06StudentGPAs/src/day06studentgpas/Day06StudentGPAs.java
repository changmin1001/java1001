/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06studentgpas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

class ValuesInvalidException extends RuntimeException {

	ValuesInvalidException(String msg) {
		super(msg);
	}
}

class PersonGrade {

	private String name;
	private double avgGrade;
	private ArrayList<String> gradeList = new ArrayList<String>();

	public void setName(String name) {
		this.name = name;
	}

	public void setAvgGrade(double avgGrade) {
		this.avgGrade = avgGrade;
	}

	public String getName() {
		return name;
	}

	public double getAvgGrade() {
		return avgGrade;
	}

	public ArrayList<String> getGradeList() {
		return gradeList;
	}

	public void setGradeList(ArrayList<String> gradeList) {
		this.gradeList = gradeList;
	}

}

public class Day06StudentGPAs {
	static String validate(String line, int index) throws ValuesInvalidException {
		if (line.isEmpty()) {
			throw new ValuesInvalidException("Line must not be empty");
		}
		if (!line.matches("[a-zA-Z]{1,50}[:]([A-F][\\+\\-]{0,1}[\\,]{0,1})+")) {
			throw new ValuesInvalidException("Line" + index + " must be made up only like----Name:A+,A ");
		}
		return line;
	}

	static double stringToGpa(String strGrade) {
		switch (strGrade) {
		case "A":
			return 4.0;
		case "A-":
			return 3.7;
		case "B+":
			return 3.3;
		case "B":
			return 3.0;
		case "B-":
			return 2.7;
		case "C+":
			return 2.3;
		case "C":
			return 2.0;
		case "D":
			return 1.0;
		case "F":
			return 0.0;
		default:
			throw new ValuesInvalidException("Invalid grade " + strGrade);
		}
	}

	static ArrayList<Double> gradesList = new ArrayList<>();

	static final String FILE_NAME = "C:\\Users\\Dell\\Documents\\java1001\\Day06StudentGPAs\\inputs.txt";
	static ArrayList<PersonGrade> stuList = new ArrayList<PersonGrade>();

	static void readDataFromFile() {

		try (Scanner fileInput = new Scanner(new File(FILE_NAME))) {
			int index = 0;
			while (fileInput.hasNextLine()) {
				try {
					String gradeInfo;
					ArrayList<String> gradeList = new ArrayList<String>();
					double sum = 0;
					PersonGrade stu = new PersonGrade();
					index++;
					String line = validate(fileInput.nextLine(), index);
					String[] firstSplit = line.split(":");
					stu.setName(firstSplit[0]);
					String[] gradeArr = firstSplit[1].split(",");
					for (int i = 0; i < gradeArr.length; i++) {
						sum += stringToGpa(gradeArr[i]);
						gradeInfo = gradeArr[i] + "(" + stringToGpa(gradeArr[i]) + ")";
						gradeList.add(gradeInfo);
					}
					stu.setGradeList(gradeList);
					stu.setAvgGrade((double) sum / gradeArr.length);
					stuList.add(stu);
				} catch (ValuesInvalidException ex) {
					System.out.println("Error parsing line: " + ex.getMessage());
				}
			}

		} catch (IOException ex) {
			System.out.println("Error reading file: " + ex.getMessage());
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		readDataFromFile();
		if (stuList.isEmpty()) {
			System.out.println("No data to process, file was empty. Exiting");
			return;
		}
		printHighest(stuList);
		try (PrintWriter fileOutput = new PrintWriter(new FileOutputStream(
				new File("C:\\Users\\Dell\\Documents\\java1001\\Day06StudentGPAs\\found.txt")))) {
			for (PersonGrade stu : stuList) {
				String gradInfo="";
				for (int i = 0; i < stu.getGradeList().size(); i++) {
					gradInfo+=(i==stu.getGradeList().size()-1?stu.getGradeList().get(i):stu.getGradeList().get(i)+", ");
				}
				System.out.println("print " + stu.getGradeList());
				fileOutput.println(stu.getName()+" has grades: "+gradInfo);
			}

		} catch (IOException ex) {
			System.out.println("Error reading file: " + ex.getMessage());
		}
	}

	static void printHighest(ArrayList<PersonGrade> list) {
		double max = list.get(0).getAvgGrade();
		int index = 0;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getAvgGrade() > max) {
				max = list.get(i).getAvgGrade();
				index = i;
			}
		}
		System.out.printf("highest name is %s and grade is %.3f : \n", list.get(index).getName(),
				list.get(index).getAvgGrade());
		
	}

}
