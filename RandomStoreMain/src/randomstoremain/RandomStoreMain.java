/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package randomstoremain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

class RandomStore {

    private int minIncl, maxExcl;
//    public RandomStore(int minIncl, int maxExcl) {
//        this.minIncl=minIncl;
//        this.maxExcl=maxExcl;
//    }

    public int nextInt(int minIncl, int maxExcl) {
        int radomnum = (int) ((Math.random() * (maxExcl - minIncl)) + minIncl);
        if (minIncl > maxExcl) {
            throw new ValuesInvalidException("min is greater than max");
        }
        intHistory.add(radomnum);
        return radomnum;
    }

    void printHistory() {
        for (int i = 0; i < intHistory.size(); i++) {

            System.out.print((i != intHistory.size() - 1) ? intHistory.get(i) + "," : intHistory.get(i));
        }
    }

    private ArrayList<Integer> intHistory = new ArrayList<>();

}

class ValuesInvalidException extends RuntimeException {

    ValuesInvalidException(String msg) {
        super(msg);
    }
}

class NumberFormatException extends InputMismatchException {

    NumberFormatException(String msg) {
        super(msg);
    }
}

public class RandomStoreMain {

    static Scanner input = new Scanner(System.in);
    static final String FILE_NAME = "people.txt";
    

    public static void main(String[] args){
        File file=new File(FILE_NAME);
        if(file.exists()){
            System.out.print("文件已经存在！");
            System.exit(0);
        }
        try (PrintWriter fileOutput=new PrintWriter(file)) {
            
            try {
                System.out.print("Input min: ");
                int minum = input.nextInt();
                System.out.print("Input max: ");
                int maxest = input.nextInt();
                try {
                    //inputNumber = input.nextInt();
                    RandomStore rs = new RandomStore();
                    for (int i = 0; i < 10; i++) {
                        int v1 = rs.nextInt(minum, maxest);
                        fileOutput.printf("%d ", v1);
                    }
                    rs.printHistory();
                } catch (ValuesInvalidException ex) {
                    System.out.println("Error: " + ex.getMessage());
                }
            } catch (InputMismatchException e) {
                System.out.println("your num is not valid！");
            }
            
        } catch (IOException ex) {
            System.out.println("Error write file: " + ex.getMessage());
        }
    }

}
